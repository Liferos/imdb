package com.ss.imdb.service.impl;

import com.ss.imdb.entity.Movie;
import org.springframework.stereotype.Service;
import com.ss.imdb.service.ServiceInterface;

/**
 * Created by Myroslav on 26.12.2017.
 */
@Service
public class MovieServiceImpl implements ServiceInterface<Movie> {

    @Override
    public Movie create(Movie obj) {
        return null;
    }

    @Override
    public Movie update(Movie obj) {
        return null;
    }

    @Override
    public Movie delete(Movie obj) {
        return null;
    }

}
