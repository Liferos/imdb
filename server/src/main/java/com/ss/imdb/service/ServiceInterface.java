package com.ss.imdb.service;

/**
 * Created by Myroslav on 26.12.2017.
 */
public interface ServiceInterface<T> {

    T create(T obj);

    T update(T obj);

    T delete(T obj);

}
