package com.ss.imdb.service.impl;

import com.ss.imdb.entity.Episode;
import org.springframework.stereotype.Service;
import com.ss.imdb.service.ServiceInterface;

/**
 * Created by Myroslav on 26.12.2017.
 */
@Service
public class EpisodeServiceImpl implements ServiceInterface<Episode> {

    @Override
    public Episode create(Episode obj) {
        return null;
    }

    @Override
    public Episode update(Episode obj) {
        return null;
    }

    @Override
    public Episode delete(Episode obj) {
        return null;
    }

}
