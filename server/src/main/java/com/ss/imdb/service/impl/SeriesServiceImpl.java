package com.ss.imdb.service.impl;

import com.ss.imdb.entity.Series;
import org.springframework.stereotype.Service;
import com.ss.imdb.service.ServiceInterface;

/**
 * Created by Myroslav on 26.12.2017.
 */
@Service
public class SeriesServiceImpl implements ServiceInterface<Series> {

    @Override
    public Series create(Series obj) {
        return null;
    }

    @Override
    public Series update(Series obj) {
        return null;
    }

    @Override
    public Series delete(Series obj) {
        return null;
    }

}
