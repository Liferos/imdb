package com.ss.imdb.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Myroslav on 26.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Movie {

    @Id @GeneratedValue @Column(name = "id") @Getter @Setter private Long id;
    @Column(name = "title") @Getter @Setter private String title;
    @Column(name = "year") @Getter @Setter private String year;
    @Column(name = "producer") @Getter @Setter private String producer;

}
