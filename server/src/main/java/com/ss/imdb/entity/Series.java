package com.ss.imdb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myroslav on 26.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Series {

    @Getter @Setter @Id @GeneratedValue @Column(name = "id") private long id;
    @Getter @Setter @Column(name = "title") private String title;
    @Getter(lazy = true) @Setter
    @JsonIgnore @OneToMany(cascade = CascadeType.ALL, mappedBy = "series", orphanRemoval = true)
    private final List<Episode> episodes = new ArrayList<>();

}
