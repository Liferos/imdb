package com.ss.imdb.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

/**
 * Created by Myroslav on 26.12.2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Episode {

    @Getter @Setter @Id @GeneratedValue @Column(name = "id") private long id;
    @Getter @Setter @Column(name = "title") private String title;
    @Getter @Setter @ManyToOne
    @JoinColumn(name = "series_id", referencedColumnName = "id", nullable = false)
    private Series series;

}
